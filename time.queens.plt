#! /gnuplot

set terminal epslatex color solid size 4, 3.5*1.5
set output "time.queens.tex"

set lmargin 3

set multiplot layout 3, 1
set tics nomirror
set border 3

set xrange [0:512+32]

set linetype 1 pointsize 2

set ytics 20
set label "$15$~Queens" right at graph 0.90, 0.85
plot "nqueens_errors.dat" index 1 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label
set ytics autofreq

set ylabel "Time (seconds)" offset -1

set label "$16$~Queens" right at graph 0.90, 0.85
plot "" index 2 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label

unset ylabel

set xlabel "Mappers"

set ytics 500
set label "$17$~Queens" right at graph 0.90, 0.85
plot "" index 3 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label
