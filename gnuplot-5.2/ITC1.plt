#! /gnuplot


set  terminal  epslatex color solid
set  output  "ITC1.tex"

set colors classic

set  size  1.17

set  view  80, 70


set  label "\\textcolor{green}{\\large DFS}"        at -2/100, -2, 345   #textcolor lt 2
set  label "\\textcolor{cyan}{\\large Iterative}"   at -2/100, -2, 286
set  label "\\textcolor{cyan}{\\large Broadening}"  at -2/100, -2, (286-40)
set  label "\\textcolor{orange}{\\large LDS}"       at -2/100, -2, 170

set  label "$\\mathsf{PieceToCover}$"  at 0.5, 170  rotate by -15
set  ylabel  "$\\mathsf{conf}$"       offset -1.2, -2.5
set  zlabel  "Solution Cost"  rotate

set  format    "$%g$"

set  xtics 0.5  offset 0.8, -0.7
set  ytics      offset 0.6, -1

set  xrange  [] reverse
set  yrange  [] reverse

set  xyplane  relative 0

set  hidden3d front

unset  colorbox


#set palette rgbformulae  7,5,15    # traditional pm3d (black-blue-red-yellow)
#set palette rgbformulae  3,11,6    # green-red-violet
#set palette rgbformulae  23,28,3   # ocean (green-blue-white); try also all other permutations
#set palette rgbformulae  21,22,23  # hot (black-red-yellow-white)
#set palette rgbformulae  30,31,32  # color printable on gray (black-blue-violet-yellow-white)
#set palette rgbformulae  33,13,10  # rainbow (blue-green-yellow-red)
#set palette rgbformulae  34,35,36  # AFM hot (black-red-yellow-white)

#set palette defined ( 50 "black", 110 "blue", 210 "magenta", 230 "red", 310 "light-magenta" )


splot  \
  "ITC1POPS.dat"    using ($2/100):3:4  with pm3d              notitle,  \
  "ITC1DFS.dat"     using ($2/100):3:4  with lines linetype 1  notitle,  \
  "ITC1IBROAD.dat"  using ($2/100):3:4  with lines linetype 4  notitle,  \
  "ITC1LDS.dat"     using ($2/100):3:4  with lines linetype 7  notitle,  \

#  "ITC1DFS_RANDVAL.dat"  using 2:3:4  with lines  title "Random DFS"


set  output
