#! /gnuplot

set size 1.17
# Manually redraw the y-axis as its end is broken when resizing the plot!
set arrow from 0, graph 0 to 0, graph 1 nohead

set label 1 "$k$" at graph 0.4, graph 1.3, graph 0.0
set label 2 "Dataset" at graph -0.3, graph 0.8, graph 0.0 rotate by 25
set zlabel "Solution Cost" rotate

set ztics 2000

set view 52, 223

unset key
unset colorbox
set xtics offset 0,-0.5
set ytics offset 0,-0.5
set ticslevel 0
min = 0
col = 14

DATA = ""
DATA2 = ""
PALETTE = "set palette defined ("

pr(x, y) = sprintf("%f %f\n", x, y)
zero_line(x, y) = DATA.sprintf("\n").DATA2.sprintf("\n%f %f\n", x, y)
zero_pal(x) = sprintf("%d %.3f %.3f %.3f", x, rand(0), rand(0), rand(0))

f(x, y) = ($0 == 0 ? (DATA = zero_line($1, x), DATA2 = pr($1, min), PALETTE = PALETTE.zero_pal(y).", ") : \
                     (DATA = DATA.pr($1, x), DATA2 = DATA2.pr($1, min)), x)

plot for [i=2:col+1] "Costs.dat" using 1:(f(column(i), i))

DATA = DATA.sprintf("\n").DATA2

set print "temporary.costs.dat"
print DATA
set print

eval(PALETTE.zero_pal(col+2).")")

set terminal epslatex color solid
set output "Costs.tex"

splot for [i=0:col-1] "temporary.costs.dat" every :::(2*i)::(2*i+1) using 1:(i):2:(i+2) with pm3d
