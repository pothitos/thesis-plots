#! /gnuplot


set  terminal  epslatex color solid
set  output    "costsA.tex"

set colors classic

set  size  1.17

set  xlabel   "$\\mathsf{conf}$"
set  ylabel   "Solution Cost"

set  format x  "$%g$"
set  format y  "$%g$"

plot  "costs.dat"  using 1:3   title "\\textsf{Ing0203-2}"  with linespoints, \
               ""  using 1:4   title "\\textsf{Ing0304-1}"  with linespoints, \
               ""  using 1:10  title "\\textsf{Ing0304-3}"  with linespoints, \
               ""  using 1:11  title "\\textsf{Ing0405-2}"  with linespoints, \
               ""  using 1:14  title "\\textsf{Ing0506-3}"  with linespoints, \
               ""  using 1:15  title "\\textsf{Ing0708-1}"  with linespoints


# comp02.ctt	Ing0203-2
# comp03.ctt	Ing0304-1
# comp09.ctt	Ing0304-3
# comp10.ctt	Ing0405-2
# comp13.ctt	Ing0506-3


set  output
