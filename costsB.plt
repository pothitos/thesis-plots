#! /gnuplot


set  terminal  epslatex color solid
set  output    "costsB.tex"

set colors classic

set  size  1.17

set  xlabel   "$\\mathsf{conf}$"
set  ylabel   "Solution Cost"

set  format x  "$%g$"
set  format y  "$%g$"

plot  "costs.dat"  using 1:2   title "\\textsf{Fis0506-1}"  with linespoints, \
               ""  using 1:5   title "\\textsf{Ing0405-3}"  with linespoints, \
               ""  using 1:6   title "\\textsf{Let0405-1}"  with linespoints, \
               ""  using 1:7   title "\\textsf{Ing0506-1}"  with linespoints, \
               ""  using 1:8   title "\\textsf{Ing0607-2}"  with linespoints, \
               ""  using 1:9   title "\\textsf{Ing0607-3}"  with linespoints, \
               ""  using 1:12  title "\\textsf{Fis0506-2}"  with linespoints, \
               ""  using 1:13  title "\\textsf{Let0506-2}"  with linespoints


# comp01.ctt	Fis0506-1
# comp04.ctt	Ing0405-3
# comp05.ctt	Let0405-1
# comp06.ctt	Ing0506-1
# comp07.ctt	Ing0607-2
# comp08.ctt	Ing0607-3
# comp11.ctt	Fis0506-2
# comp12.ctt	Let0506-2
# comp14.ctt	Ing0708-1


set  output
