#! /gnuplot

set terminal epslatex color solid size 4, 3.5*1.5
set output "time.partitioning.tex"

set lmargin 3

set multiplot layout 3, 1
set tics nomirror
set border 3

set xrange [0:512+32]

set linetype 1 pointsize 2

set label "$N\\!=\\!40$ Partitioning" right at graph 0.90, 0.85
plot "numpart_errors.dat" index 1 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label

set ylabel "Time (seconds)" offset -1

set label "$N\\!=\\!44$ Partitioning" right at graph 0.90, 0.85
plot "" index 2 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label

unset ylabel

set xlabel "Mappers"

set ytics 4000
set label "$N\\!=\\!48$ Partitioning" right at graph 0.90, 0.85
plot "" index 3 using 2:3:4:xtic($2==32?"32\\hspace{1em}":stringcolumn(2)) with errorlines pointsize 0 notitle
unset label
