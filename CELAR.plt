#! /gnuplot


set  terminal  epslatex color solid  size 5.8, 7
set  output    "CELAR.tex"

set colors classic

#set  size  0.67, 0.67

set  xrange  [0:130]
set  format x  ""
set  format y  "$%g$"


#set  key  samplen 0
set tics nomirror
set border 3

#unset  xtics
#set    grid

set  tmargin  0
set  bmargin  0.5

set  lmargin  6
set  rmargin  1


set  multiplot layout  5, 1


set  yrange  [280000:350000]
set  ytics  20000
set  label  "\\textsf{SCEN07}"  at graph 0.83, 0.35
plot  "CELAR.dat"  using 1:($3/1000)  notitle  with linespoints linestyle 1
unset  label


set  yrange  [9500:12500]
set  ytics  1000
set  label  "\\textsf{SCEN10}"  at graph 0.83, 0.30
plot  ""           using 1:($6/1000)  notitle  with linespoints linestyle 2
unset  label


set  ylabel   "Solution Cost (thousands)"  offset -4

set  yrange  [460:540]
set  ytics  20
set  label  "\\textsf{SCEN09}"  at graph 0.83, 0.40
plot  ""           using 1:($5/1000)  notitle  with linespoints linestyle 3
unset  label

unset  ylabel


set  yrange  [165:195]
set  ytics  10
set  label  "\\textsf{SCEN06}"  at graph 0.83, 0.35
plot  ""           using 1:($2/1000)  notitle  with linespoints linestyle 4
unset  label


set  xtics  nomirror
set  format x  "$%g$"

set  bmargin  2
set  xlabel   "$\\mathsf{conf}$"

set  yrange  [8.3:8.7]
set  ytics  0.2
set  label  "\\textsf{SCEN08}"  at graph 0.83, 0.75
plot  ""           using 1:($4/1000)  notitle  with linespoints linestyle 5
